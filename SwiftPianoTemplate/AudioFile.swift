//
//  AudioFile.swift
//  SwiftPianoTemplate
//
//  Created by Ankit Mishra on 16/03/18.
//  Copyright © 2018 csh. All rights reserved.
//

import AVFoundation

class AudioFile {
    
    let audioFilesName = ["A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7"]
    var audioPlayers:[AVAudioPlayer] = []
    
    // Load Files
    init() {
        
        for file in audioFilesName {
            
            let filePath = Bundle.main.path(forResource: file, ofType: "mp3")
            let url = URL (fileURLWithPath: filePath!)
            
            do {
                let player = try AVAudioPlayer (contentsOf: url)
                player.prepareToPlay()
                audioPlayers.append(player)
            }catch{
                print("File Loading - \(error)")
            }
        }
    }
    
    
    func playSound(tag: Int) {
        
        let t = ((tag - 100) % 8)
        audioPlayers[t].currentTime = 0
        audioPlayers[t].play()
    }
    
    func stopSound(tag:Int) {
        let t = ((tag - 100) % 8)
        audioPlayers[t].stop()
    }
    
    
}
