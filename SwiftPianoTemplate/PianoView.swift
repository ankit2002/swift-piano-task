//
//  PianoView.swift
//  SwiftPianoTemplate
//
//  Created by Ankit Mishra on 15/03/18.
//  Copyright © 2018 csh. All rights reserved.
//

import UIKit


public protocol PianoDelegate: class {
    func keyUp(tag: Int)
    func keyDown(tag: Int)
}

class PianoView : UIView {
    
    // Variable Declaration
    private var numberofKeys: Int = 24
    private var numberOfWhiteKeys: Int = 0
    private var keysArray : [UIButton?] = []
    public weak var delegate: PianoDelegate?
    
    // Initalization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .lightGray
        initializeLayoutDesign()
    }
    
    // Deserialize -- restoring state
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeLayoutDesign()
    }
    
     // Method for Screen Design
    func initializeLayoutDesign(){
       
        numberOfWhiteKeys = 0
        // counting total white keys
        for i in 1 ..< numberofKeys + 1 {
            if checkWhiteKey(i){
                numberOfWhiteKeys += 1
            }
        }
        keysArray = [UIButton?](repeating: nil, count: (numberofKeys + 1))
        isMultipleTouchEnabled = true
        
        // remove all subviews
        for v in subviews{
            v.removeFromSuperview()
        }
    }
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        initializeLayoutDesign()
        
        // starting position of Button
        var x: CGFloat = 0.0
        
        // size of Keys
        let whiteKeyWidth: CGFloat = (bounds.size.width / CGFloat(numberOfWhiteKeys))
        let whiteKeyHeight: CGFloat = bounds.size.height
        let blackKeyHeight = bounds.size.height * 0.60
        let blackKeyWidth = whiteKeyWidth * 0.80
        let blackKeyShift = blackKeyWidth / 2.0
        
        // Create White Keys
        for i in 0 ..< numberofKeys {
            
            if checkWhiteKey(i){
                
                let newx = (x + 0.5)
                let key = UIButton(frame: CGRect(x: newx, y: -5, width: whiteKeyWidth , height:whiteKeyHeight - 10))
                key.backgroundColor = .white
                key.setBackgroundColor(.lightGray, for: .highlighted)
                key.tag = (100 + i)
                key.layer.cornerRadius = 5
                key.layer.borderWidth = 0.5
                key.layer.borderColor = UIColor.black.cgColor
                key.addTarget(self, action: #selector(self.buttonPressed(_:)), for: .touchDown)
                key.addTarget(self, action: #selector(self.buttonReleased(_:)), for: .touchUpInside)
                key.addTarget(self, action: #selector(self.buttonReleased(_:)), for: .touchUpOutside)
                self.addSubview(key)
                keysArray[i] = key
                x = (x + whiteKeyWidth + 0.5)
            }
        }
            
        // Create Black Keys
        x = 0.0
        for i in 0 ..< numberofKeys {
            if checkWhiteKey(i) {
                x += whiteKeyWidth
            } else {
                let keyRect = CGRect(x: (x - blackKeyShift), y: -5, width: blackKeyWidth, height: blackKeyHeight)
                let key = UIButton(frame: keyRect)
                key.backgroundColor = .black
                key.setBackgroundColor(.lightGray, for: .highlighted)
                key.tag = (100 + i)
                key.layer.cornerRadius = 5
                key.layer.borderWidth = 0.5
                key.layer.borderColor = UIColor.black.cgColor
                key.addTarget(self, action: #selector(self.buttonPressed(_:)), for: .touchDown)
                key.addTarget(self, action: #selector(self.buttonReleased(_:)), for: .touchUpInside)
                key.addTarget(self, action: #selector(self.buttonReleased(_:)), for: .touchUpOutside)
                self.addSubview(key)
                keysArray[i] = key
            }
        }
    }
    
    func checkWhiteKey(_ num: Int) -> Bool {
        let qou = num % 12
        return (qou == 0 || qou == 2 || qou == 4 || qou == 5 || qou == 7 || qou == 9 || qou == 11)
    }
    
    @objc func buttonPressed(_ sender: UIButton){
        delegate?.keyDown(tag: Int(sender.tag))
    }
    
    @objc func buttonReleased(_ sender: UIButton){
        delegate?.keyUp(tag: Int(sender.tag))
    }
}


extension UIButton {
    func setBackgroundColor(_ color: UIColor, for state: UIControlState) {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let currentGraphicsContext = UIGraphicsGetCurrentContext() {
            currentGraphicsContext.setFillColor(color.cgColor)
            currentGraphicsContext.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        }
        layer.masksToBounds = true
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        setBackgroundImage(colorImage, for: state)
    }
}
