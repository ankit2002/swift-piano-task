//
//  MainViewController.swift
//  SwiftPianoTemplate
//
//  Created by Chris on 01.03.18.
//  Copyright © 2018 csh. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, PianoDelegate {
    
    var audioEngine : AudioFile!
    var pianoView: PianoView!
    
    override public func loadView() {
        let rect = UIScreen.main.bounds.size
        self.pianoView = PianoView(frame: CGRect(x: 0, y: 0, width: rect.width , height:rect.height))
        super.view = pianoView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.audioEngine = AudioFile()
        self.pianoView.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Stop audio signal
    func keyUp(tag: Int) {
        //audioEngine?.stopSound(tag: tag)
    }
    
    // Play audio Signal
    func keyDown(tag: Int) {
        audioEngine?.playSound(tag: tag)
    }
}

